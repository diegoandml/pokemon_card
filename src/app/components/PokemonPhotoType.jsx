import React from 'react'

const styles = {
  photoTypeSize: {
    height: '4.5em',
  }
}


function PokemonPhotoType(props) {
  return <img style={styles.photoTypeSize} src={props.image} />
}

export default PokemonPhotoType