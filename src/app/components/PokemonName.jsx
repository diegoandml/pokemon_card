import React from 'react'

const styles = {
  background: {
    backgroundColor: 'azure'
  },
  titleHead: {
    color: 'red'
  }
}

function PokemonName(props) {
  return (
    <div style={styles.background}>
      <h1 style={styles.titleHead}>{props.name}</h1>
    </div>
  )
}

export default PokemonName