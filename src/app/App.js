import React from 'react';
import pokemon from '../assets/images/pokemon-fire.png'

import PokemonName from './components/PokemonName'
import PokemonPhoto from './components/PokemonPhoto'
import PokemonPhotoType from './components/PokemonPhotoType'

const styles = {
  pokemonName: {
    display: 'flex'
  }
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <PokemonPhotoType image={pokemon} />
        <div style={styles.pokemonName}>
          <PokemonName name="Chikorita" />
          <PokemonPhoto width="100px !important"  name="Chikorita" photo="https://assets.pokemon.com/assets/cms2/img/pokedex/full/152.png" />
        </div>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
